/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   Segmento.cpp
 * Author: pathfind
 * 
 * Created on 18 de Dezembro de 2017, 08:37
 */

#include "Segmento.h"

Segmento::Segmento() 
{

}
Segmento::Segmento(string nomeMapa_, int id_, double length_, float kph_, string oneway_)
{
    this->nomeMapa = nomeMapa_;
    this->id = id_;
    this->length = length_;
    this->kph = kph_;
    this->oneway = oneway_;
}

Segmento::Segmento(const Segmento& orig) 
{
    this->id = orig.id;
    this->kph = orig.kph;
    this->length = orig.length;
    this->nomeMapa = orig.nomeMapa;
    this->oneway = orig.oneway;
}

Segmento::~Segmento() 
{

}

bool Segmento::operator==(const Segmento& other)
{
    if(this->id != other.id)
        return false;
    if(this->kph != other.kph)
        return false;
    if(this->length != other.length)
        return false;
    if(this->oneway != other.oneway)
        return false;
    return true;
}

bool Segmento::segMapasIguais(const Segmento& other)
{
    if(this->nomeMapa == other.nomeMapa)
        return true;
    return false;
}

void Segmento::setOneway(string oneway) {
    this->oneway = oneway;
}

string Segmento::getOneway(){
    return oneway;
}

void Segmento::setKph(float kph) {
    this->kph = kph;
}

float Segmento::getKph(){
    return kph;
}

void Segmento::setLength(double length) {
    this->length = length;
}

double Segmento::getLength(){
    return length;
}

void Segmento::setId(double id) {
    this->id = id;
}

double Segmento::getId(){
    return id;
}

void Segmento::setNomeMapa(string nomeMapa) {
    this->nomeMapa = nomeMapa;
}

string Segmento::getNomeMapa(){
    return nomeMapa;
}

void Segmento::print()
{
    cout << "ID: " << this->id << endl;
    cout << "LENGTH: " << this->length << endl;
    cout << "KPH: " << this->kph << endl;
    cout << "NOME DO MAPA: " << this->nomeMapa << endl;
    cout << "DIRECAO: " << this->oneway << endl;
}