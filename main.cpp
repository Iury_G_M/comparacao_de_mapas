/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   main.cpp
 * Author: pathfind
 *
 * Created on 15 de Dezembro de 2017, 15:09
 */

#include <cstdlib>
#include <vector>
#include <map>
#include <string>
#include <iostream>
#include <string.h>
#include <fstream>
#include <iomanip>

#include "shapelib/shapefil.h"

#include "Segmento.h"

using namespace std;

#define ARQUIVO_1 "brabr2"
#define ARQUIVO_2 "brabr1"

/**
 * Guarda em um arquivo, os segmentos que estao em uma unica malha, assim como a malha na qual estes segmentos estao contidos.
 * @param informacao <b>map<id, vector<Segmento> ></b> com as informacoes dos segmentos de cada malha.
 */
void segEmUmaUnicaMalha(map<double, vector<Segmento> >& informacao)
{
    vector<double> idArquivo1(0); //Vetor para os ids do arquivo 1.
    vector<double> idArquivo2(0); //Vetor para os ids do arquivo 2.
    
    //Guardam o id dos segmentos que ocorrem em uma unica malha no vetor de sua malha.
    for(map<double, vector<Segmento> >::iterator it = informacao.begin(); it != informacao.end(); it++)
    {
        if(it->second.size() == 1)
        {
            if(it->second.back().getNomeMapa() == ARQUIVO_1)
            {
                idArquivo1.push_back(it->first);
            }
            if(it->second.back().getNomeMapa() == ARQUIVO_2)
            {
                idArquivo2.push_back(it->first);
            }
        }        
    }
        
    string nomeDoArquivo = ("Segmentos_Unicos.txt");
    
    ofstream arquivo;
    
    cout << endl;
    
    //Deletar o arquivo, se ja existir.
    if(remove(nomeDoArquivo.c_str()) == 0)
    {
        cout << "Arquivo " + nomeDoArquivo + " deletado." << endl;
    }
    

    //Cria um novo arquivo.
    arquivo.open(nomeDoArquivo.c_str());
    
    //Preenche o arquivo.
    arquivo << ARQUIVO_1 << " - " << idArquivo1.size() << endl;
    for(int i = 0; i < idArquivo1.size(); i ++)
    {
        arquivo << setprecision(20) << idArquivo1[i] << " ";
    }
    arquivo << "\n";

    arquivo << ARQUIVO_2 << " - " << idArquivo2.size() << endl;
    for(int i = 0; i < idArquivo2.size(); i ++)
    {
        arquivo << setprecision(20) << idArquivo2[i] << " ";
    }
    arquivo << "\n";
    
    //Fecha o arquivo.
    arquivo.close();
}

/**
 * Imprime em um arquivo, o id dos segmentos que possuem velocidades diferentes.
 * @param informacao map com os dados dos segmentos de cada mapa.
 */
void velocidadeDiferente(map<double, vector<Segmento> >& informacao)
{
    vector<double> id(0);
        
    //Adiciona, em um vetor, o id dos segmentos que possuem kph diferentes.
    for(map<double, vector<Segmento> >::iterator it = informacao.begin(); it != informacao.end(); it++)
    {   
        if(it->second.size() == 2)
        {
            if(it->second[0].getKph() != it->second[1].getKph() && it->second[0].getNomeMapa() != it->second[1].getNomeMapa())
            {
                id.push_back(it->first);
            }
        }
    }
    
    string nomeDoArquivo = "Segmentos_Com_Velocidades_Diferente.txt";
    ofstream arquivo;
    
    if(remove(nomeDoArquivo.c_str()) == 0)
    {
        cout << "Arquivo " + nomeDoArquivo + " deletado." << endl;
    }
    
    //Cria um novo arquivo.
    arquivo.open(nomeDoArquivo.c_str());
    
    //Escreve em um arquivo o id dos segmentos que possuem velocidade deiferentes.
    arquivo << "Segmentos com velocidades diferente - " << id.size() << "\n";
    for(int i = 0; i < id.size(); i ++)
    {
        arquivo << setprecision(20) << id[i] << " ";
    }
    arquivo << "\n";
    arquivo.close();
    
}

/**
 * Imprime, em um arquivo, o id dos segmentos que possuem direcao diferentes.
 * @param informacao map com os dados dos segmentos.
 */
void direcaoDiferente(map<double, vector<Segmento> >& informacao)
{
    vector<double> id(0);

    //Guarda, em um vetor, o id dos segmentos que possuem direcao diferentes nas malhas.
    for(map<double, vector<Segmento> >::iterator it = informacao.begin(); it != informacao.end(); it++)
    {   
        if(it->second.size() == 2)
        {
            if(it->second[0].getOneway() != it->second[1].getOneway() && it->second[0].getNomeMapa() != it->second[1].getNomeMapa())
            {
                id.push_back(it->first);
            }
        }
    }
    
    string nomeDoArquivo = "Segmentos_Com_Direcao_Diferente.txt";
    ofstream arquivo;
    
    if(remove(nomeDoArquivo.c_str()) == 0)
    {
        cout << "Arquivo " + nomeDoArquivo + " deletado." << endl;
    }
    
    //Cria um novo arquivo.
    arquivo.open(nomeDoArquivo.c_str());

    //Escreve o id dos segmentos, com direcoes diferentes, no arquivo
    arquivo << "Segmentos com direcao diferente - " << id.size() << "\n";
    for(int i = 0; i < id.size(); i ++)
    {
        arquivo << setprecision(20) << id[i] << " ";
    }
    arquivo << "\n";
    arquivo.close();    
}

/**
 * Imprime em um arquivo o id dos segmentos que possuem comprimento diferentes.
 * @param informacao map com os dados dos segmentos.
 */
void comprimentoDiferente(map<double, vector<Segmento> >& informacao)
{
    vector<double> id(0);

    //Adiciona em um vetor os ids dos segmentos que possuem comprimento diferente.
    for(map<double, vector<Segmento> >::iterator it = informacao.begin(); it != informacao.end(); it++)
    {   
        if(it->second.size() == 2)
        {
            if(it->second[0].getLength() != it->second[1].getLength() && it->second[0].getNomeMapa() != it->second[1].getNomeMapa())
            {
                id.push_back(it->first);
            }
        }
    }
    
    string nomeDoArquivo = "Segmentos_Com_Comprimento_Diferente.txt";
    ofstream arquivo;
    
    if(remove(nomeDoArquivo.c_str()) == 0)
    {
        cout << "Arquivo " + nomeDoArquivo + " deletado." << endl;
    }
    
    //Cria um novo arquivo.
    arquivo.open(nomeDoArquivo.c_str());
    
    //Escreve em um arquivo os ids dos segmentos com comprimentos diferentes.
    arquivo << "Segmentos com comprimento diferente - " << id.size() << "\n";
    for(int i = 0; i < id.size(); i ++)
    {
        arquivo << setprecision(20) << id[i] << " ";
    }
    arquivo << "\n";
    arquivo.close();    
    
    
}

/**
 * Interpreta os dados, para verificar as diferencas nos semgentos de duas malhas.
 * @param informacao <b>map<id, vector<Segmento> ></b> com as informacoes dos segmentos de cada malha.
 */
void interpretarDados(map<double, vector<Segmento> >& informacao)
{
    //VERIFICA QUAIS SEGMENTOS ESTAO EM APENAS UMA MALHA
    //------------------------------------------------------------------------//
    segEmUmaUnicaMalha(informacao);
    
    //VERIFICA QUAIS SEGMENTOS POSSUEM VELOCIDADE DIFERENTE
    //------------------------------------------------------------------------//
    velocidadeDiferente(informacao);
    
    //VERIFICA QUAIS SEGMENTOS POSSUEM SENTIDO DIFERENTES
    //------------------------------------------------------------------------//
    direcaoDiferente(informacao);
    
    //VERIFICA QUAIS SEGMENTOS POSSUEM COMPRIMENTOS DIFERENTES
    //------------------------------------------------------------------------//
    comprimentoDiferente(informacao);
    
}

/**
 * Guarda os dados do segmento. 
 * @param shapefile shapefile que sera lido.
 * @param banco banco de dados do shapefile.
 * @param informacao <b>map<id do segmento, vector<Segmentos>></b>
 * @param nomeDoMapa nome do mapa que sera lido
 * @param indiceDoSegmento indice do segmento, no shapefile, que sera lido.
 * @param indiceDoId indice do atributo, no shapefile, que sera lido.
 * @param indiceDoSentido indice do atributo sentido, no shapefile, que sera lido.
 * @param indiceDoComprimento indice do atributo comprimento, no shapefile, que sera lido.
 * @param indiceDaVelocidade indice do atributo velocidade , no shapefile, que sera lido.
 */
void lerDadosDoSegmento(SHPHandle& shapefile, DBFHandle& banco, map<double, vector<Segmento> >& informacao, string& nomeDoMapa, int indiceDoSegmento, int indiceDoId, int indiceDoSentido, int indiceDoComprimento, int indiceDaVelocidade)
{
    SHPObject *shape;
    
    shape = SHPReadObject(shapefile, indiceDoSegmento);
    
    if( shape == NULL ) {
        std::cerr<< "Arquivo " << nomeDoMapa << ".shp : Incapaz de ler o shape " << indiceDoSegmento << ", passando para o proximo.\n" << std::endl;
        return ;
    }
    
    double comprimento;
    float velocidade;
    double id;
    string sentido; 
    
    //Le os dados do segmento.
    comprimento = DBFReadDoubleAttribute(banco, indiceDoSegmento, indiceDoComprimento);
    velocidade = (float)(DBFReadDoubleAttribute(banco, indiceDoSegmento, indiceDaVelocidade));
    id = DBFIsAttributeNULL(banco, indiceDoSegmento, indiceDoId) ? 0 :DBFReadDoubleAttribute(banco, indiceDoSegmento, indiceDoId);
    sentido = DBFReadStringAttribute(banco, indiceDoSegmento, indiceDoSentido);
    if(id < 0)
    {
        cout << DBFReadStringAttribute(banco, indiceDoSegmento, indiceDoId) << endl;
    }
//    DBFIsAttributeNULL(dbf, i, seg_eid_fld) ? 0 : DBFReadDoubleAttribute(dbf, i, seg_eid_fld)
//    if(-2147483648 == id)
//        cout <<endl;
    
    //Cria um objeto segmento
    Segmento segmento(nomeDoMapa, id, comprimento, velocidade, sentido);
    
    //Verifica se existe uma chave no mapa igual ao id.
    map< double, vector<Segmento> >::iterator iterador = informacao.find(id);
    //Se o mapa nao possuir um campo com valor igual ao id.
    if(iterador != informacao.end())
    {
        informacao[id].push_back(segmento);
    }
    //Caso contrario
    else
    {
        //Adiciona o segmento ao vetor na posicao que possui chave id no mapa.
        vector<Segmento> vetor(0);
        vetor.push_back(segmento);
        informacao.insert(make_pair(id, vetor));
    }
    
}

/**
 * Le os dados no shapefile e os guarda no mapa de segmentos, informacao.
 * @param shapefile shapefile que sera lido.
 * @param banco banco de dados do shapefile.
 * @param informacao <b>map<id, vector<Segmentos>></b>:mapa de segmentos que contera as informacoes de cada segmento.
 * @param nomeDoMapa nome do shapefile que sera lido.
 */
void lerDadosDoMapa(SHPHandle& shapefile, DBFHandle& banco, map<double, vector<Segmento> >& informacao, string& nomeDoMapa)
{
    cout << "Lendo o shapefile " << nomeDoMapa << "." << std::endl;
    
    int indiceComprimento;
    int indiceVelocidade;
    int indiceID;
    int indiceSentido;
    
    //Guarda os indices das colunas, no shapefile, dos dados necessarios.
    indiceComprimento = DBFGetFieldIndex(banco, "METERS");
    indiceVelocidade  = DBFGetFieldIndex(banco, "KPH");
    indiceID  = DBFGetFieldIndex(banco, "ID");
    indiceSentido  = DBFGetFieldIndex(banco, "ONEWAY");    
    
    if(indiceComprimento == -1 || indiceID == -1 || indiceSentido == -1 || indiceVelocidade == -1)
    {
        std::cerr << "Nao foi possivel ler o arquivo " << nomeDoMapa << "." << std::endl;
        return ;
    }
    else
    {
        int tipoDeShape, totalDeShapes;
	double limiteInferior[4], limiteSuperior[4];
        
        //Guarda o numero total de segmentos na variavel totalDeSegmentos.
	SHPGetInfo( shapefile, &totalDeShapes, &tipoDeShape, limiteInferior, limiteSuperior);

        int last_pct = -1;
        cout << "# total de segmentos: " << totalDeShapes << endl;
        for(int segmento = 0; segmento < totalDeShapes; segmento ++)
        {
            int pct = (100 * segmento) / (totalDeShapes-1);
            if (pct != last_pct) {
            	last_pct = pct;
		std::cout << "\r" << pct << "%" << " done..." << std::flush;
            } 
            lerDadosDoSegmento(shapefile, banco, informacao, nomeDoMapa, segmento, indiceID, indiceSentido, indiceComprimento, indiceVelocidade);
        }
    }
    
    cout << "Leitura do shapefile " << nomeDoMapa << " finalizada." << std::endl;
    
}

/** 
 * Abre os arquivos .shp e .dbf de acordo com o nome do arquivo.
 * @param shp .shp que sera aberto.
 * @param dbf .dbf que sera aberto.
 * @param filename nome do .dbf e do .shp que serao abertos.
 * @return returna true se os arquivos foram abertos com sucesso e false caso contrario.
 */
bool abrirMapa(SHPHandle& shp, DBFHandle& dbf, const char *filename)
{
    //Ler o shapefile como arquivo binario.
    shp = SHPOpen(filename, "rb" );
    
    //Nao e possivel ler o arquivo.
    if( shp == NULL ) {
    	std::cerr << "Unable to open .shp file: %s.shp\n" << filename << std::endl;
	return false;
    }

    //Ler o .dbf como arquivo binario
    dbf = DBFOpen(filename, "rb");

    if( dbf == NULL ) {
	std::cerr << "Unable to open .dbf file: %s.dbf\n" << filename << std::endl;
	SHPClose( shp );
	return false;
    }
    
    cout << endl;
    
    cout << "Arquivo " << filename << " aberto." << endl;
    return true;
}

/**
 * Le o dado relevante das 2 malhas e os guarda no mapa informacao.
 * @param informacao mapa onde os dados de cada segmento devera ser guardado. A chave do mapa representa o id do segmento e o valor é um vetor com a informacao relevante. 
 */
void compararMapa(map<double, vector<Segmento> >& informacao)
{
    
    string nomeDoArquivo = ARQUIVO_1;
    nomeDoArquivo = "MAPAS/" + nomeDoArquivo;
    
    //------------------------------------------------------------------------//
    
    //Abre o mapa com nome igual ao ARQUIVO_1.
    SHPHandle arquivo1SHP;
    DBFHandle arquivo1DBF;
    
    abrirMapa(arquivo1SHP, arquivo1DBF, nomeDoArquivo.c_str());
    
    //Le os dados da malha e os guarda no map.
    nomeDoArquivo = ARQUIVO_1;
    lerDadosDoMapa(arquivo1SHP, arquivo1DBF, informacao, nomeDoArquivo);
    
    SHPClose(arquivo1SHP);
    DBFClose(arquivo1DBF);  
    
    cout << "Arquivo " << nomeDoArquivo << " fechado." << endl;
    //------------------------------------------------------------------------//

    SHPHandle arquivo2SHP;
    DBFHandle arquivo2DBF;

    //Abre o mapa com o nome igual ao ARQUIVO_2.
    nomeDoArquivo = ARQUIVO_2;
    nomeDoArquivo = "MAPAS/" + nomeDoArquivo;
    
    abrirMapa(arquivo2SHP, arquivo2DBF, nomeDoArquivo.c_str());
    
    //Le os dados da malha e os guarda no map.
    nomeDoArquivo = ARQUIVO_2;
    lerDadosDoMapa(arquivo2SHP, arquivo2DBF, informacao, nomeDoArquivo);

    SHPClose(arquivo2SHP);
    DBFClose(arquivo2DBF);
    
    cout << "Arquivo " << nomeDoArquivo << " fechado." << endl;
    //------------------------------------------------------------------------//

}

int main(int argc, char** argv) {

    map<double, vector<Segmento> > informacao;
    
    compararMapa(informacao);
    
    interpretarDados(informacao);
    
    return 0;
}

