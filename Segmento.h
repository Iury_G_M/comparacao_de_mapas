/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   Segmento.h
 * Author: pathfind
 *
 * Created on 18 de Dezembro de 2017, 08:37
 */

#ifndef SEGMENTO_H
#define SEGMENTO_H

#include <string>
#include <iostream>

using namespace std;

class Segmento {
public:

    /**
     * Construtor padrao. Nao faz nada.
     */
    Segmento();

    /**
    * Construtor utilizado para criar um segmento.
    * @param nomeMapa_ nome do mapa onde os dados de <b>*this</b> foram obtidos
    * @param id_ id do segmento.
    * @param length_ comprimento do segmento.
    * @param kph_ velocidade do segmento.
    * @param oneway_ sentido do segmento.
    * @return nao possui retorno.
    */
    Segmento(string nomeMapa_, int id_, double length_, float kph_, string oneway_);

    /**
     * Cria um novo segmento com os mesmos dados do segmento orig.
     * @param orig segmento que tera seus dados copiados.
     */
    Segmento(const Segmento& orig);

    /**
     * Destrutor. Nao faz nada.
     */
    virtual ~Segmento();

    /**
    * Metodo utilizado para verificar se dois segmentos sao iguais.
    * @param other segmento que sera comparado com o <b>*this</b>.
    * @return <b>true:</b> se os dois segmentos forem iguais; <b>false:</b> caso contrario.
    */
    bool operator==(const Segmento& other);

    /**
    * Verifica se dois segmentos sao de mapas iguais.
    * @param other segmento que sera comparado com o <b>*this</b>.
    * @return <b>true:</b> se os dois segmentos pertencem ao mesmo mapa; <b>false:</b> caso contrario.
    */
    bool segMapasIguais(const Segmento& other);
    
    void setOneway(string oneway);
    string getOneway();
    void setKph(float kph);
    float getKph();
    void setLength(double length);
    double getLength();
    void setId(double id);
    double getId();
    void setNomeMapa(string nomeMapa);
    string getNomeMapa();
    void print();

private:
    string nomeMapa;
    double id;
    double length;
    float kph;
    string oneway;
};

#endif /* SEGMENTO_H */
